SET CLASSPATH="./config/;lib/*"
SET JVM_PARAMS=-Xms128m -Xmx640m
SET ENV_VARIABLES=-Dfile.encoding=UTF-8 -DNotificadorCompromissos=true 
SET MAIN_CLASS=br.com.galgo.notificadorformalizados.NotificadoFormalizadosMain

echo on

JAVA %JVM_PARAMS% -cp %CLASSPATH% %ENV_VARIABLES% %MAIN_CLASS%

pause
