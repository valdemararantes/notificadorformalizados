package br.com.galgo.notificadorformalizados.email;

import br.com.galgo.notificadorformalizados.Controller;
import br.com.galgo.notificadorformalizados.ReportManager;
import br.com.galgo.notificadorformalizados.model.JobExtracaoBaseCompromissoBO;
import br.com.galgo.notificadorformalizados.model.JobExtracaoBaseCompromissoStatusEnum;
import br.com.galgo.notificadorformalizados.model.ListaEmailProvedoresCompromissoBO;
import br.com.galgo.notificadorformalizados.model.ServicoEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import static br.com.galgo.notificadorformalizados.model.JobExtracaoBaseCompromissoStatusEnum.valueOf;

public class JobExtracaoBaseCompromissoFileUtils {
    private static final String FILE_ENCODING = "Cp1252";
    private static final String CVS_SPLITBY = ";";
    private static final List<ServicoEnum> servicosFiltro;
    static Logger log = LoggerFactory.getLogger(JobExtracaoBaseCompromissoFileUtils.class);

    static {
        final String servicos_filtro_Str = Controller.mapOfProperties.getProperty("SERVICOS_FILTRO");
        if (servicos_filtro_Str == null || servicos_filtro_Str.trim() == "") {
            servicosFiltro = null;
        } else {
            final String[] servicosFiltrosStrArray = servicos_filtro_Str.split(",");
            servicosFiltro = new ArrayList<>(servicosFiltrosStrArray.length);
            for (String servicoFiltroStr : servicosFiltrosStrArray) {
                if (servicoFiltroStr == null || servicoFiltroStr.trim() == "") {
                    continue;
                } else {
                    servicosFiltro.add(ServicoEnum.getAsEnum(servicoFiltroStr.trim()));
                }
            }
        }
    }


    /**
     * Metodo que processa o arquivo CSV STIPRDB_Job_Extracao_Base_Compromisso
     * Como premissa o arquivo tem que estar com encoding Cp1252 e separado por ;
     * Ao ler o arquivo, um filtro por status de compromisso é aplicado.
     *  Além do filtro por status, nem todas as colunas sao relevantes no processamento do arquivo, sendo 
     *  assim, é levado em consideracao somente as colunas: A a H, J, K, X, AC a AE
     *  As colunas de AC a AE, só são preenchidas no arquivo quando o a coluna de serviço for do tipo EXTRATO_DE_CONCILIACAO_DE_COTAS.
     * @param filePath Path do arquivo a ser lido.
     * @throws IOException
     * @throws ParseException
     */
    public static void processFile(Path filePath) throws IOException, ParseException {
        log.info("Dando inicio ao processamento do job de compromissos'");

        final String csvFile = filePath.toString();
        String line = "";

        try (BufferedReader br = new BufferedReader(
            new InputStreamReader(new FileInputStream(csvFile), JobExtracaoBaseCompromissoFileUtils.FILE_ENCODING))) {
            int count = 0;
            while ((line = br.readLine()) != null) {
                count++;
                // A primeira linha é exatamente o HEADER dos CSVs a serem enviados às instituições
                if (count == 1) {
                    ReportManager.FILE_HEADER = line + "\r\n";
                    continue;
                }


                //split da linha 
                String[] splitedCurrentLine = line.split(CVS_SPLITBY);

                // Prossigo apenas se passa pelo filtro de serviços
                if (servicosFiltro != null && !servicosFiltro.isEmpty()) {
                    final ServicoEnum comprServico = ServicoEnum.getAsEnum(splitedCurrentLine[7]);
                    boolean filterOk = false;
                    for (ServicoEnum servFiltro : servicosFiltro) {
                        if (servFiltro == comprServico) {
                            filterOk = true;
                            break;
                        }
                    }
                    if (!filterOk) { // Prossegue para a próxima linha do arquivo
                        continue;
                    }
                }

                JobExtracaoBaseCompromissoStatusEnum statusCompromisso = valueOf(splitedCurrentLine[2]);
                final String[] formalizadoStatusList = Controller.getInstance().getProperty("FORMALIZADO_STATUS_LIST").split(",");
                if (Stream.of(formalizadoStatusList).map(JobExtracaoBaseCompromissoStatusEnum::valueOf).
                    anyMatch(status -> statusCompromisso.equals(status))) {

                    JobExtracaoBaseCompromissoBO extracaoBO = new JobExtracaoBaseCompromissoBO();

                    //Necessário porque mais à frente será criado um Map[key=servico, value=List[JobExtracaoBaseCompromissoBO]] baseado neste campo
                    if (ServicoEnum.getAsEnum(splitedCurrentLine[7]) == ServicoEnum.INFORMACOES_ANBIMA) {
                        extracaoBO.setServico(ServicoEnum.PL_COTA); //H
                    } else {
                        extracaoBO.setServico(ServicoEnum.getAsEnum(splitedCurrentLine[7])); //H
                    }

                    // Preciso salvar recuperar também a entidade provedora e a consumidora, para que consiga enviar os e-mails
                    extracaoBO.setNomeEntidadeProvedora(splitedCurrentLine[3]);
                    extracaoBO.setNomeEntidadeConsumidora(splitedCurrentLine[5]);
                    // extracaoBO.setEntidadeAprovadora(splitedCurrentLine[23]);

                    //As linhas no CSV são iguais às do arquivo extraído PerfLabel IBM
                    extracaoBO.setLine(line);

                    JobExtracaoBaseCompromissoBO.addInReportStructure(extracaoBO);
                }
            }
        } catch (FileNotFoundException e) {
            log.error("Erro ao processar job de compromisso: " + filePath.toString(), e);
            throw e;
        } catch (IOException e) {
            log.error("Erro ao processar job de compromisso: " + filePath.toString(), e);
            throw e;
        }

        log.info("Arquivo processado com sucesso: " + filePath.toString());
    }

    /**
     * Metodo que lê o arquivo de emails, recebe como parametro uma string com o caminho do arquivo.
     * @param filePath o caminho completdo do arquivo
     * @throws IOException caso algum erro de IO aconteca.
     */
    public static void readEmailsFile(String filePath) throws IOException {

        ListaEmailProvedoresCompromissoBO.clearStatic();
        String csvFile = filePath;
        String line = "";
        String csvSplitBy = ";";

        try (BufferedReader br = new BufferedReader(
            new InputStreamReader(new FileInputStream(csvFile), JobExtracaoBaseCompromissoFileUtils.FILE_ENCODING))) {
            int count = 0;
            while ((line = br.readLine()) != null) {
                count++;
                if (count == 1) {
                    continue;
                }
                String nomeEntidadeProvedora = "";
                try {
                    String[] splitedCurrentLine = line.split(csvSplitBy);
                    nomeEntidadeProvedora = splitedCurrentLine[0];
                    ServicoEnum servicoProvido = ServicoEnum.getAsEnum(splitedCurrentLine[1]);
                    String strEmail = splitedCurrentLine[2];
                    ListaEmailProvedoresCompromissoBO.addEmailEntidadeProvedora(
                        nomeEntidadeProvedora, servicoProvido, strEmail);
                } catch (Exception e) {
                    log.info(
                        "Serviço ou e-mail não encontrado para Entidade: " + nomeEntidadeProvedora + " na linha: "
                            + count);
                    continue;
                }
            }
        } catch (FileNotFoundException e) {
            log.error("Erro ao processar job de compromisso: " + filePath, e);
            throw e;
        } catch (IOException e) {
            log.error("Erro ao processar job de compromisso: " + filePath, e);
            throw e;
        }
        log.info("Arquivo processado com sucesso: " + filePath);
    }

    /**
     * Metodo que converte uma string data em um objeto do tipo data de acordo com o formato desejado.
     * @param date2Contert a data em formato de string que se deseja converter
     * @param format a string de formato que se deseja a data
     * @return um objeto do tipo Data formatado
     * @throws ParseException caso nao consegua parsear a data parametrizada.
     * @author Eduardo.Calderini
     */
    private static Date convertString2Date(String date2Contert, String format) throws ParseException {
        if (date2Contert == null || date2Contert.equals("")) {
            return null;
        }

        DateFormat formatter = new SimpleDateFormat(format);
        Date date = (Date) formatter.parse(date2Contert);

        return date;
    }

}
