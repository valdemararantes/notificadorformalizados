package br.com.galgo.notificadorformalizados.model;

public enum JobExtracaoBaseCompromissoStatusEnum {

    SOLICITACAO_APROVADA,
    SOLICITACAO_CANCELADA,
    SOLICITACAO_REJEITADA,
    COMPROMISSO_A_FORMALIZAR,
    SOLICITACAO_AGUARDANDO_CONFIRMACAO_ALTERACAO,
    SOLICITACAO_AGUARDANDO_VALIDACAO,
    SOLICITACAO_AGUARDANDO_APROVACAO,
    SOLICITACAO_AGUARDANDO_APROVACAO_ALTERACAO_PROPOSTA,
    COMPROMISSO_FORMALIZADO,
    COMPROMISSO_FINALIZADO,
    COMPROMISSO_SUSPENSO,
    SOLICITACAO_AGUARDANDO_APROVACAO_REATIVACAO,
    COMPROMISSO_A_INICIAR,
    COMPROMISSO_CANCELADO,
    SOLICITACAO_AGUARDANDO_CONFIRMACAO_SUSPENSAO;

    /**
     * Utilizar valueOf
     * @param strEnum
     * @return
     */
    @Deprecated
    public static JobExtracaoBaseCompromissoStatusEnum getAsEnum(String strEnum) {
        return valueOf(strEnum);
    }

/*
    public static JobExtracaoBaseCompromissoStatusEnum getAsEnum_OLD(String strEnum) {
        if (strEnum.equals("SOLICITACAO_APROVADA")) {
            return SOLICITACAO_APROVADA;
        }

        if (strEnum.equals("SOLICITACAO_CANCELADA")) {
            return SOLICITACAO_CANCELADA;
        }

        if (strEnum.equals("SOLICITACAO_REJEITADA")) {
            return SOLICITACAO_REJEITADA;
        }

        if (strEnum.equals("COMPROMISSO_A_FORMALIZAR")) {
            return COMPROMISSO_A_FORMALIZAR;
        }

        if (strEnum.equals("SOLICITACAO_AGUARDANDO_CONFIRMACAO_ALTERACAO")) {
            return SOLICITACAO_AGUARDANDO_CONFIRMACAO_ALTERACAO;
        }

        if (strEnum.equals("SOLICITACAO_AGUARDANDO_VALIDACAO")) {
            return SOLICITACAO_AGUARDANDO_VALIDACAO;
        }

        if (strEnum.equals("SOLICITACAO_AGUARDANDO_APROVACAO")) {
            return SOLICITACAO_AGUARDANDO_APROVACAO;
        }

        if (strEnum.equals("SOLICITACAO_AGUARDANDO_APROVACAO_ALTERACAO_PROPOSTA")) {
            return SOLICITACAO_AGUARDANDO_APROVACAO_ALTERACAO_PROPOSTA;
        }

        if (strEnum.equals("SOLICITACAO_APROVADA")) {
            return SOLICITACAO_APROVADA;
        }

        if (strEnum.equals("COMPROMISSO_FINALIZADO")) {
            return COMPROMISSO_FINALIZADO;
        }

        if (strEnum.equals("SOLICITACAO_APROVADA")) {
            return SOLICITACAO_APROVADA;
        }

        if (strEnum.equals("SOLICITACAO_AGUARDANDO_APROVACAO_REATIVACAO")) {
            return SOLICITACAO_AGUARDANDO_APROVACAO_REATIVACAO;
        }

        if (strEnum.equals("COMPROMISSO_A_INICIAR")) {
            return COMPROMISSO_A_INICIAR;
        }

        if (strEnum.equals("COMPROMISSO_CANCELADO")) {
            return COMPROMISSO_CANCELADO;
        }

        return null;
    }
*/
}

class Teste {
    public static void main(String[] args) {
        System.out.println("oi");
        final JobExtracaoBaseCompromissoStatusEnum comp = JobExtracaoBaseCompromissoStatusEnum.valueOf("COMPROMISSO_CANCELADO");
        final JobExtracaoBaseCompromissoStatusEnum comp1 = JobExtracaoBaseCompromissoStatusEnum.getAsEnum("COMPROMISSO_CANCELADO");
        System.out.println("comp=" + comp + "; " + (comp == comp1));
    }
}